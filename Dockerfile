FROM scratch
ADD nrp-controller /
CMD ["/nrp-controller"]
EXPOSE 80